package com.human.lw4;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    Button clickedPairBtn;
    Button clickedBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void onButtonClick(View view) {
        Button button = (Button) findViewById(R.id.b1);
        button.setText("Changed");
        Toast.makeText(MainActivity.this, "Button pushed", Toast.LENGTH_SHORT).show();
    }

    public void onGroupBtnClick(View view) {
        clickedBtn = (Button) view;
        clickedBtn.setText("this");
    }

    public void  onSecondGroupBtnClick(View view) {
        Button button = findViewById(view.getId());
        button.setText("this");
    }

    public void onPairsBtnsClick(View view) {
        if (view != clickedPairBtn) {
        Button clickedBtn = (Button) view;
        Toast.makeText(MainActivity.this, "PairButton pushed", Toast.LENGTH_SHORT).show();
        clickedPairBtn = clickedBtn;
        }
    }
}
